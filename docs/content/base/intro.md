---
title: Introducción
---

# Introducción

[[toc]]

## Aplicacion

**Alzate** tiene como principal alcance apoyar en la distribucion de presupuesto de los costo que implica mantener la infraestructura que da soporte el departamento de **IT** al grupo empresarial, esto se realiza baso el ejercio de presupuestoi anual al cual se le aplican reglas de distribucion segun sea el cueadro de servicio y los distintos drivers que tenga un producto.

### Estado

En desarrollo ~ 70 - 80% (por confirmar)

## Tecnologia

- NET Framwork 4.5+
- SQL SERVER
- Webforms
- Devexpress
- Git

## Configuracion
### VPN
- **REMOTE GATEWAY**: vpn.fanalca.com.co
- **PORT**: 443
- **USER**: innova4j
- **PASSWORD**:  F4n4Inn0v41920

### Base de Datos

- **IP**: 172.18.5.103
- **PORT**: 49377
- **USER**: usr_medeski
- **PASSWORD**: t1234
  
### Acceso al Sistema

- **USER**: Developer
- **PASSWORD**: Fanalca2018
  
## Convenciones
### Git
#### Git flow

La metodoligia de versionamiento que se estara aplicando sera git flow (_[Cheat Sheet](https://danielkummer.github.io/git-flow-cheatsheet/)_)
