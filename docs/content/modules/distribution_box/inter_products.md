---
title: Productos Intermedios
---

# Productos Intermedios

Configuracion de Productos Intermedios

## Acciones

### Guardar

Guarda la configuracion de la distribucion de un producto intermedio.

![An image](../../../img/service_box/form_prod_inter.png)

## Estado

**Funciona**

## Observaciones

Sin Observaciones