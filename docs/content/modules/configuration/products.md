---
title: Productos
---

# Productos

Configuracion de productos e items del mismo

[[toc]]

![An image](../../../img/products/list_products.png)

## Acciones

### Registrar/Editar
- En el layout
- En la tabla

Redireciona a un formulario donde se registra la informacion del producto

![An image](../../../img/products/form_product.png)

### Ver Listado de Items(Por producto)

Lista todos los items creados para el producto _**[ver mas](##Items)**_

## Items

![An image](../../../img/products/form_product.png)

### Acciones

#### Registrar/Editar

Redireciona a un formulario donde se registra la informacion del item

![An image](../../../img/products/form_item.png)

## Estado

**Funcionando**

## Observaciones

En la tabla del listado de items de un producto la primera columna no posee un titulo adecuado al campo, deberia al menos describirlo de la sigiente formar _(sugerencia)_ **"Tipo de Item"** .
