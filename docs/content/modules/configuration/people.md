---
title: Personas
---

# Personas

Configuracion de personas y delegados

[[toc]]

## Modulos

### Personas

![An image](../../../img/people/people_list.png)

#### Aciones

##### Registro/Edicion (Persona)

- **En el layout**
- **En la tabla**

Redirecciona a un formulario donde se registra la informacion base del empleado.

![An image](../../../img/people/form_people.png)

> Al Registrar o editar muestra un mensaje que nos pide que confirmemos si queremos _**"asignar un costo a esta persona para el periodo activo"**_.

![An image](../../../img/people/succes_addedit_message.png)

##### Registro/Edicion (Costo de Persona por Periodo Activo)

![An image](../../../img/people/form_people_cost.png)

#### Estado

Funcionando

#### Observaciones

- Si Observaciones

### Delegados

![An image](../../../img/people/delgate_list.png)

#### Acciones

##### Asignar

Redireciona a un formulario con un dropdownlist que carga informacion de la fase.

![An image](../../../img/people/delgate_form.png)

#### Estado

- Falla / _Por confirmar comportamiento deseado_

#### Observaciones

Al selecionar una fase y guardar el registro arroja un mensaje en el cual especifica que se seleccione un delegado, pero en su defecto ningun delegado es cargado del lado del cliente.

- Mensaje de Error
  
![An image](../../../img/people/delagate_error_message.png)

- Formulario despues de la seleccion de la fase (_No se ve presente ningun dropdonwlist u otro control del parte del cliente que permita dicha selecion_)

![An image](../../../img/people/delgate_form_after_phase_selected.png)

##### Observaciones (Revision de Codigo)

Del lado de la vista se observa un contro del tipo checkbox con el **ID="cbLPersonas"** el cual es el contro que se debe mostrar al selecionar una fase.

**formDelegados_form.aspx**

~~~~
<dx:ASPxCheckBoxList 
    ID="cbLPersonas" 
    CssClass="form-control delegados" 
    runat="server" 
    ValueField="pers_consecutivo" 
    TextField="pers_nombres" 
    RepeatColumns="2" 
    RepeatLayout="OrderedList" 
    Caption="" 
    RepeatDirection="Vertical" 
    Theme ="MetropolisBlue">
        <CaptionSettings Position="Top" />
</dx:ASPxCheckBoxList>
~~~~

**formDelegados_form.aspx.cs**

Del lado del _codebehind_de la vista existe un metodo **CargarListas** el cual es ejecutado al iniciar la pagina o en cada _postback_ actualmente no carga informacion con el usuario utilizado **Developer**.

~~~~
cbLPersonas.DataSource = null;
cbLPersonas.DataSource = Ctrpersonas.GetAllActiveJefeOrderBy(strUsuario[0]); // 'Developer'
cbLPersonas.DataBind();
~~~~



