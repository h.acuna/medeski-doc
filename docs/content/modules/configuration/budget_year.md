---
title: Año Presupuesto
---

# Año Presupuesto

Configuracion del año del presupuesto

[[toc]]

  ![An image](../../../img/budget_year/list.png)


## Acciones

### Registro/Edicion
- **En el layout**
- **En la Tabla**


  Ambas acciones redireccionan a un formulario donde se puede seleccionar el año año de presupuesto, indicar el estado del año(_activo, inactivo_) y se se duplican los parametros(_opcional_). 
  > En caso de selecionar que se dupliquen los parametros se debera selecionar el año y la version de la cual se requiere duplicar los parametros

  ![An image](../../../img/budget_year/form.png)


## Estado

Funcional

## Observaciones

Al registrar o editar un presupuesto no regresa al modulo principal donde se lista todos los años de presupuesto