---
title: Compañia
---

# Compañia

Configuracion - TODO

[[toc]]

## Centros de Operaciones

  ![An image](../../../img/company/list.png)

### Acciones Registrar/Editar
- En el Layout
- En la Tabla
  Redireciona a un formulario en el cual se registra o se edita la informacion del centro de costo.

  ![An image](../../../img/company/form_cost_center.png)

### Estado

**Funcionando**

### Observaciones 

Sin Observaciones

## Compañias - Esencial

Registro de compañias para la generacion de la distribucion del presupuesto asignar

  ![An image](../../../img/company/companies_list.png)

### Acciones

#### Registro/Edicion

Registra o se edita la informacion esencial para la compañia, en la cual se puede registrar una compañia sin asignarle un centro de costo luego de su registro.

> **Nota** ver observaciones

  ![An image](../../../img/company/form_company.png)

#### Detalles Centros de Costo

## Funciona ~ **Defectos**
- Permite asignarle registrar un centro de costo a una compañia que se especifica tanto en la edicion como su registro que **"No posee centro de cotos?"**-
## Observaciones
- Inhabilitar la opcion de centro de costo a las compañias que se registra como que no posee(Evaluar)
- Que eventos se requeriria manejar en caso de cambiar la opcion que la compañia cuando posee centros de costo asociado y esta se le cambia la opcion a **No** posee centros de costo
- Como impacta en la distribucion que una compañia no posea al menos un centro de costo.







