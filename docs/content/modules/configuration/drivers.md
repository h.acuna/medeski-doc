---
title: Drivers
---

# Drivers

Configuracion de Drivers

[[toc]]

![An image](../../../img/dirver/list.png)

## Acciones

### Registrar/Editar
- En el Layout
- En la tabla

Redirecciona a un formulario donde se registrar o editar la informacion del driver.

![An image](../../../img/dirver/list.png)

## Estado

Funcionando

## Observaciones

Sin Observaciones
