---
title: Cambio Moneda
---

# Cambio Moneda

Configuracion de cambio de moneda.

[[toc]]

![An image](../../../img/currency_exchange/list.png)

## Acciones
### Ver Detalles(Listado de Meses)
Muestra los detalles de la configuracion de los valores de la moneda para cada mes del año

![An image](../../../img/currency_exchange/list_currency_mounths.png)
### Acciones(Listado de Meses)

#### Edicion - (_Falla o comportamiento no definido_)

![An image](../../../img/currency_exchange/form.png)

> Al tratar de hacer la valor para el mes selecionado en el listado, siempre que se quiere modificar el valor o el cambiar el estatus la aplicacion siempre retorna el siguiente mensaje.

![An image](../../../img/currency_exchange/error_message.png)

## Estado

**Por Validar**

## Observaciones

En caso de que los valores de la tabla no sean editables eliminar tan funcionabilidad o confirmar cual seria el escenario en el cual si se permita la edicion(**_Por Validar_**).

