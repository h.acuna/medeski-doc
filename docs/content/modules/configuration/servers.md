---
title: Servidores
---

# Servidores

Configuracion de servidores

[[toc]]

![An image](../../../img/servers/list.png)

## Acciones

### Registro/Edicion

Redirecciona a un formulario donde se registra la informacion del servidor.

![An image](../../../img/servers/form.png)

## Estado

- Falla / Al registrar

## Observaciones

- Muchos campos presentes en el formulario deberian tener un tabla maestras donde se permite elaborar un mejor relacion para la informacion que se registra para el sevidor.

![An image](../../../img/servers/form_observations.png)