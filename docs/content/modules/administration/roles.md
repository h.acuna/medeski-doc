---
title: Roles
---

# Roles

Modulo de administracion de roles

[[toc]]

![An image](../../../img/administration/_layout.png)

## Acciones
### Generales

Redireciona a un formulario para registrar un nuevo rol _~ Pasa lo mismo en el caso de la edicion_

![An image](../../../img/administration/register_new_role.png)

### En la tabla

### Editar
![An Image](../../../img/general/edit.png) Redireciona a un formulario para registrar un nuevo rol _~ Pasa lo mismo en el caso del registro_


## Estado

- Funcionando

## Observaciones

Se repite el mensaje de de _"Registro satisfactorio"_ tando para los eventos de agregar un nuevo role como para la edicion del mismo.

![An Image](../../../img/general/success_message.png)

> **Nota**: se sugiere manejar un mensaje distinto para cada evento.