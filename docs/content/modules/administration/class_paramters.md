---
title: Clases Parametros
---

# Clases

Modulo de administracion de Clases

[[toc]]

![An image](../../../img/administration/class_layout.png)

## Acciones
### Generales

Redireciona a un formulario para registrar un nueva Clases _~ Pasa lo mismo en el caso de la edicion_

![An image](../../../img/administration/register_new_class.png)

### En la tabla

### Editar

![An Image](../../../img/general/edit.png) Redireciona a un formulario para registrar un nueva Clases _~ Pasa lo mismo en el caso del registro_

### Ver Parametros

![An Image](../../../img/general/details.png) Redirecciona a la lista de parametros que contiene la clase



## Estado

- Funcionando

## Observaciones

Se repite el mensaje de de _"Registro satisfactorio"_ tando para los eventos de agregar un nuevo Clasese como para la edicion del mismo.

![An Image](../../../img/general/success_message.png)

> **Nota**: se sugiere manejar un mensaje distinto para cada evento