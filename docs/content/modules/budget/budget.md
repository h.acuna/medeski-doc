---
title: Presupuesto
---

# Presupuesto

Modulo de presupuesto

[[toc]]

![An image](../../../img/budget/list.png)

## Aciones

### Registrar

Redirecion a un formulario donde se registrara la informacion del presupuesto

![An image](../../../img/budget/list.png)

## Estado

**Falla** _Puntualmente_

## Observaciones

- Error de tipo de datos al selecionar el primer dato en dropdownlist de compañias. _(Solo en caso de selecionar la subcategoria cuentas especiales)_
  
![An image](../../../img/budget/error_parse.png)

Se agregara algo.

