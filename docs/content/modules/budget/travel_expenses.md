---
title: Gastos Viaje
---

# Gastos Viaje

[[toc]]

Modulo de gastos de viaje

![An image](../../../img/budget/list_travel_list.png)

## Acciones

### Registro/Edicion

Redireciona a un formulario en donde se registrara la informacion del gasto del viaje.

![An image](../../../img/budget/form_travel.png)

## Estado

**Funciona**

## Observaciones

Sin Observaciones
