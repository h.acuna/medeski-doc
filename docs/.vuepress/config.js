module.exports = {
    title: 'Fanalca - Alzate',
    description: 'Revision de Modulos del Sistema',
    lastUpdated: 'Last Updated',
    port: 8081,
    serviceWorker: {
      updatePopup: true // Boolean | Object, default to undefined.
      // If set to true, the default text config will be: 
      // updatePopup: { 
      //    message: "New content is available.", 
      //    buttonText: "Refresh" 
      // }
    },
    head: [
        ['link', { rel: 'icon', href: `/Logotipo_Medeski.fw.png` }]
    ],
    themeConfig: {
        editLinks: true,
        displayAllHeaders: true,
        sidebar: [
          {
            title: 'Inicio',
            collapsable: false,
            children: [ 
                'content/base/intro',
                'content/base/scope',
             ]
          },
          {
            title: 'Modulo - Administracion',
            collapsable: false,
            children: [
                'content/modules/administration/roles',
                'content/modules/administration/class_paramters',
            ]
          },
          {
            title: 'Modulo - Configuracion',
            collapsable: false,
            children:[
                'content/modules/configuration/budget_year',
                'content/modules/configuration/currency_exchange',
                'content/modules/configuration/people',
                'content/modules/configuration/company',
                'content/modules/configuration/products',
                'content/modules/configuration/drivers',
                'content/modules/configuration/servers'
            ]
          },
          {
            title: 'Modulo - Presupuesto',
            collapsable: false,
            children:[
                'content/modules/budget/budget',
                'content/modules/budget/travel_expenses',
            ]
          }
        ],
        docsDir: 'docs'
    }
}